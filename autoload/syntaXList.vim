let s:attributes={'NONE': '', 'bold':'', 'italic':'', 'strikethrough':'', 'underline':''}

function! syntaXList#ListSettings()
    " Loop over syntax  and show coloring

    " for documentation
    "let s:attributes={'NONE': '', 'bold':'', 'italic':'', 'strikethrough':'', 'underline':''}
    "let s:prefices={'ctermfg':'', 'ctermbg':'', 'guifg':'', 'guibg':'', 'cterm':''}

    let l:introOut="        ******* SYNTAX SETTINGS ********* \n\n\n"
   
    let l:stdColors="# Colors \n\n"
    let l:stdColors=l:stdColors . "   -b:syntaXColorOn: \t"   . get(b:, "syntaXColorOn", 1) . " (sets standard colors)\n\n"

    let l:stdColors=l:stdColors . "   -b:syntaXstart: \t\t<blue>" . get(b:, "syntaXstart", "NOT_SET") . "</end>\n\n"
    let l:stdColors=l:stdColors . "   -b:syntaXsend: \t\t<blue>" . get(b:, "syntaXend", "NOT_SET") . "</end>\n\n"

    if exists("b:syntaXColors")
        let l:counter=1
        let l:stdColors=l:stdColors . "   -@NAME@ (defined in b:syntaXColors):\n\n\t"
        for l:entry in b:syntaXColors 
            if type(l:entry) == 3
                if len(l:entry) == 2
                    let l:stdColors=l:stdColors . l:entry[0]
                else
                    let l:stdColors=l:stdColors . "INVALID_LEN(" . len(l:entry) . ")"
                endif
            else
                let l:stdColors=l:stdColors . "INVALID_ENTRY_TYPE(" . type(l:entry) .")"
            endif
            if l:counter % 5 == 0
                let l:stdColors=l:stdColors . "\n\t"
            else
                let l:stdColors=l:stdColors . " \t \t \t"
            endif

            let l:counter=l:counter + 1
        endfor
    else
        let l:stdColors=l:stdColors . "NO COLORS DEFINED\n"
    endif


    let l:attr="\n\n# Standard Attributes \n\n"
    if exists("b:syntaXAttrTags")
        if type(b:syntaXAttrTags) == 3
            let l:attr=l:attr . "   - b:syntaXStdAttrOn: ". get(b:, "syntaXStdAttrOn", 1) . "\n\n"
            let l:attr=l:attr . "   - Tags (<START>, <END>, <EFFECT>): \n\n"
            for l:entry in b:syntaXAttrTags
                if len(l:entry) == 3
                    if has_key(s:attributes, l:entry[2])
                        let l:attr=l:attr . "\t " . string(l:entry) . "\n"
                    else
                        let l:attr=l:attr . "\t<INVALID>" . string(l:entry) . "</end>\n"
                    endif
                else
                    let l:attr=l:attr . "<INVALID>" . string(l:entry) . " -invalid</END>\n"
                endif
            endfor
        else
            let l:attr=l:attr . "<INVALID>b:syntaXAttrTags is not a list</END>\n"
        endif
    else
        let l:attr=l:attr . "<NOT_SET>No standard attribute tags set</END>\n"
    endif


    let l:colorAttr="\n\n# Color Attributes \n\n"
    let l:colorAttr=l:colorAttr . "   - b:syntaXColorAttrOn: ". get(b:, "syntaXColorAttrOn", 1) . "\n\n"

    if exists("b:syntaXColorAttr") 
        if type(b:syntaXColorAttr) == 4
            for [l:attrC, l:syntax] in items(b:syntaXColorAttr)
                let l:colorAttr=l:colorAttr . "\t" . l:attrC .<SID>AddTabs(l:attrC, 4) . "<blue>" . l:syntax . "</end>\n"
            endfor  
        else
            let colorAttr=l:colorAttr . "b:syntaXColorAttr not a dictionary"
        endif
    else
        let colorAttr=l:colorAttr . "b:syntaXColorAttr not defined"
    endif

    let l:etc="\n\n# Etc \n\n"
    let l:etc=l:etc . "   - b:syntaXdelim: " . get(b:, "syntaXdelim", "<NOT_SET>'/' ( default)</end>") . "\n\n"

    " Output in a new buffer (after the cunnt buffer settings are evaluted and
    " formatted)
    let l:syntaXColors=b:syntaXColors
    execute ":new"
    execute "syntax keyword testy Color"
    execute "highlight testy ctermfg=10"
    silent put = l:introOut
    silent put = l:stdColors
    silent put = l:attr
    silent put = l:colorAttr
    silent put = l:etc

    let b:syntaXstart="<@NAME@>"
    let b:syntaXend="</end>"

    set ft=markdown
    call <SID>colorizeKeyword(l:syntaXColors)

    call syntaX#Extend()
    set conceallevel=3
    set concealcursor=n
    set ro
    set nomodifiable
    execute "normal! gg"

endfunction

function <SID>AddTabs(input, tabs)
    let l:tabwidth=&tabstop

    let l:tabsWidthC=len(a:input)/&tabstop
    let l:tabsNm=a:tabs -  l:tabsWidthC

    return  repeat("\t", l:tabsNm)

endfunction

function <SID>colorizeKeyword(syntaXColors)
    if type(a:syntaXColors) != 3 
        echoerr "<SID>colorizeKeyWord:: incorrect usage" 
        return 1
    endif

    "Highlight color names in their respective color
    for l:entry in a:syntaXColors
        if type(l:entry) == 3 && len(l:entry) == 2
            if type(l:entry[0]) == 1 && type(l:entry[1]) == 4
                execute "syntax keyword" l:entry[0] l:entry[0] "containedin=ALL"

                let hlAttr=""
                for [l:attr, l:value] in items(l:entry[1])
                    let l:hlAttr=l:hlAttr . " " . l:attr . "=" . l:value
                endfor
                execute "highlight" l:entry[0] l:hlAttr
            else
                echoerr "malformed entry for syntaXColors ([<name>, <dict>]) : " type(l:entry[0]) && type(l:entry[1])
            endif
        else
            echoerr "malformed entry for syntaXColors ([<name>, <dict>]) : " string(l:entry)
        endif
    endfor

    " Higlight parameters on and off
    syntax keyword ON 1
    syntax keyword OFF 0
    highlight ON ctermfg=10
    highlight OFF ctermfg=1
    
    " Highlight NOT_SET and INVALID
    syntax region NOT_SET matchgroup=conceal start=+<NOT_SET>+ end=+b:syntaXend+  concealends
    syntax region INVALID matchgroup=conceal start=+<INVALID>+ end=+b:syntaXend+ concealends
    highlight NOT_SET ctermfg=11
    highlight INVALID ctermfg=9

endfunction
