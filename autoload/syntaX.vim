let s:colorNames=[]
let s:highlight=[]

let s:attributes={'NONE': '', 'bold':'', 'italic':'', 'strikethrough':'', 'underline':''}
let s:prefices={'ctermfg':'', 'ctermbg':'', 'guifg':'', 'guibg':'', 'cterm':''}

"Extended syntax should be enabled after syntax on is called
"Otherwise the syntax list is overwritten
"autoloading from a script delays loading until the buffer
"is entered and presumably syntax is set on
function! syntaX#Enable()
    augroup syntaxEnter
        autocmd BufEnter <buffer> call syntaX#Extend()
    augroup syntaxEnter
endfunction

function! syntaX#Extend()
    " DEBUG {
    echom "Loading extended syntax"
    "syntax keyword Function test
    "call <SID>syntaxtest()
    " } END DEBUG
    autocmd! syntaxEnter BufEnter <buffer> 

    if get(b:, "syntaXdisable", 0) == 1
        return 0
    endif

    call <SID>setMissing()

    " Set basic syntax and highlighting
    if get(b:, "syntaXColorOn", 1) == 1
        if exists("b:syntaXColors") &&  exists("b:syntaXstart") &&  exists("b:syntaXend") &&  exists("b:syntaXdelim")

            " Add syntax and highlights without attribute
            let l:syntaXColors=<SID>loadSyntax(b:syntaXColors, b:syntaXstart, b:syntaXend, b:syntaXdelim, ['NONE', "NONE"])
            call <SID>loadHighLight(l:syntaXColors)

            " Add syntax and highlights with attribute
            if get(b:, "syntaXColorAttrOn", 1)
                if type(b:syntaXColorAttr) == 4
                    for [l:key, l:value] in items(b:syntaXColorAttr)
                        let l:syntaXColors=<SID>loadSyntax(b:syntaXColors, b:syntaXstart, b:syntaXend, b:syntaXdelim, [l:key, l:value])
                        call <SID>loadHighLight(l:syntaXColors)                                                                               "for l:attr in b:syntaxColors
                    endfor
                else
                    echoerr "b:syntaXColorAttr (" b:syntaXColorAttr ")not a dict"
                endif
            endif
        else
            echoerr "b:syntaXColors, b:syntaXstart, b:syntaXend or b:syntaXdelim not defined"
        endif
    endif
    
    " Set basic attribute syntax and highlighting
    if get(b:, "syntaXStdAttrOn", 1) == 1
        if exists("b:syntaXAttrTags") && exists("b:syntaXdelim")
            call <SID>addStdAttr(b:syntaXAttrTags, b:syntaXdelim)
        else
            echoerr "syntaXStdAttrOn but b:syntaXAttrTags or b:syntaXdelim not defined"
        endif
    endif
endfunction

function! <SID>loadSyntax(syntaXColors, start, end, delim, attr)

    let l:syntaXColors=deepcopy(a:syntaXColors)

    "argument checking
    if type(l:syntaXColors) != 3 && type(a:start) != 1 && type(a:end) != 1 && len(a:attr) != 2
        echoerr "<SID>loadSyntax: incorrect usage" len(l:syntaXColors) type(a:start) type(a:end) len(a:attr)
        return 1
    endif

    for l:entry in l:syntaXColors
        if type(l:entry) == 3 && len(l:entry) == 2
" Constructing a syntax entry
" Example:
" :syntax region syntaX_[name] matchgroup=conceal start=[delim][start][delim]  end=[delim][end][delim] concealends
            if type(l:entry[0]) == 1 && type(l:entry[1]) == 4
                let l:hlName="syntaX_" . l:entry[0]
                let l:subString=l:entry[0]
                if a:attr[0] !=# "NONE"
                    let l:hlName=l:hlName . "_" . a:attr[0]
                    let l:subString=l:subString . a:attr[1]
                    let l:entry[1]['cterm']=a:attr[0]
                endif
                
                let l:startSub=substitute(a:start, "@NAME@", l:subString,"g")
                let l:endSub=substitute(a:end, "@NAME@", l:subString, "g")
                let l:entry[0]=l:hlName

                let l:syntaxEntry=":syntax region " . l:hlName . " matchgroup=conceal" . 
                            \ " start=" . a:delim . l:startSub . a:delim . 
                            \ " end="  .  a:delim . l:endSub   . a:delim . " concealends" . " containedin=ALL"
                execute l:syntaxEntry
            else
                echoerr "malformed entry for syntaXColors ([<name>, <dict>]) : " type(l:entry[0]) && type(l:entry[1])
            endif
        else
            echoerr "malformed entry for syntaXColors ([<name>, <dict>]) : " string(l:entry)
        endif
    endfor

    return l:syntaXColors

endfunction

function! <SID>loadHighLight(syntaXColors)
    set conceallevel=2

    "echom string(a:syntaXColors)
    "return 0

    if type(a:syntaXColors) != 3
        echoerr "<SID>loadHighlight: incorrect usage " len(a:syntaXColors) 
        return 1
    endif

    for l:entry in a:syntaXColors
       let l:syntaXhlCommand="highlight "

       if type(l:entry) == 3 && len(l:entry) == 2
           if type(l:entry[0]) == 1 && type(l:entry[1]) == 4
               " Add groupname to hl command
               let l:syntaXhlCommand=l:syntaXhlCommand . l:entry[0] . " "
               for [l:key, l:value] in items(l:entry[1])
                   
                   "echom "DEBUG key: " l:key
                   "echom "DEBUG value: " l:value

                   if <SID>isValidHLEntry(l:key, l:value)
                       let l:syntaXhlCommand=l:syntaXhlCommand . l:key . "=" . l:value . " "
                   else
                       echoerr "Invalid highlight entry: " l:key ", " l:value
                       let l:syntaXhlCommand=""
                       break
                   endif
               endfor
               "echom "DEBUG: " l:syntaXhlCommand
               execute l:syntaXhlCommand

           else
               echoerr "malformed entry for syntaXColors ([<name>, <dict>]) : " type(l:entry[0]) && type(l:entry[1])
           endif
       else
           echoerr "malformed entry for syntaXColors ([<name>, <dict>]) : " string(l:entry)
       endif

     endfor

endfunction

function! <SID>isValidHLEntry(prefix, value)
    if ! has_key(s:prefices, a:prefix)
        echoerr "Unrecognized prefix: " a:prefix " (" keys(s:prefices) ")"
        return 0
    endif

    if a:prefix == "cterm"
        if !has_key(s:attributes, a:value)
            echoerr "Unrecognized attribute: " a:value" (" keys(s:attributes) ")"
            return 0
        endif

    elseif a:prefix ==# "ctermfg" || a:prefix ==# "ctermbg"
        if !(( a:value >= 0 && a:value <= 255 ) || a:value ==# "NONE" )
            echoerr "Invalid value: " a:value " (0-255) for prefix: " a:prefix
            return 0
        endif
    elseif a:prefix ==# "guibg" || a:prefix ==# "guifg"
        if a:value ==# "NONE"
            return 1
        endif

        if !len(a:value) > 1
            echoerr "Invalid value: " a:value " (#000000-#FFFFFF|NONE) for prefix: " a:prefix
            return 0
        endif

        if !(a:value[0] == "#")
            echoerr "Invalid value: " a:value " (#000000-#FFFFFF|NONE) for prefix: " a:prefix
            return 0
        endif

        let l:hex="0x" . a:value[1:]
        if !(l:hex < 0x0 && l:hex > 0xFFFFFF)
            echoerr "Invalid value: " a:value " (#000000-#FFFFFF|NONE) for prefix: " a:prefix
            return 0
        endif

    else
        echoerr "Unexpected error: Unparsed prefix"
        return 0
    endif
    return 1
endfunction


function! <SID>addStdAttr(syntaXAttrTags, delim)

    if type(a:syntaXAttrTags) != 3
        echoerr "Requires list"
    endif

    for l:attrPair in a:syntaXAttrTags
        if len(l:attrPair) == 3 
            if has_key(s:attributes, l:attrPair[2])
                let l:hlName="syntaX_" . l:attrPair[2]
                let l:start=l:attrPair[0]
                let l:end=l:attrPair[1]

                let l:syntaxEntry=":syntax region " . l:hlName . " matchgroup=conceal" . 
                            \ " start=" . a:delim . l:start . a:delim . 
                            \ " end="  .  a:delim . l:end . a:delim . " concealends"
                execute l:syntaxEntry
                execute ":highlight " l:hlName " cterm="l:attrPair[2]
            else
                echoerr l:attrPair[2] " unknown attribute. See :help syntaX (|attributes|)"
            endif
        else
            echoerr "l:attrPair does not have 3 elements" l:attrPair
        endif
    endfor
endfunction

function! <SID>setMissing()

    if !exists("b:syntaXColors")
     let b:syntaXColors=
                \[['black'  , {'ctermfg' :'0' , 'ctermbg':'NONE', 'guifg':'NONE', 'guibg':'NONE', 'cterm':'NONE'}],
                \ ['white'  , {'ctermfg' :'15', 'ctermbg':'NONE', 'guifg':'NONE', 'guibg':'NONE', 'cterm':'NONE'}],
                \ ['gray'   , {'ctermfg' :'8' , 'ctermbg':'NONE', 'guifg':'NONE', 'guibg':'NONE', 'cterm':'NONE'}],
                \ ['silver' , {'ctermfg' :'7' , 'ctermbg':'NONE', 'guifg':'NONE', 'guibg':'NONE', 'cterm':'NONE'}],
                \ ['maroon' , {'ctermfg' :'1' , 'ctermbg':'NONE', 'guifg':'NONE', 'guibg':'NONE', 'cterm':'NONE'}],
                \ ['red'    , {'ctermfg' :'9' , 'ctermbg':'NONE', 'guifg':'NONE', 'guibg':'NONE', 'cterm':'NONE'}],
                \ ['purple' , {'ctermfg' :'5' , 'ctermbg':'NONE', 'guifg':'NONE', 'guibg':'NONE', 'cterm':'NONE'}], 
                \ ['fushia' , {'ctermfg' :'13', 'ctermbg':'NONE', 'guifg':'NONE', 'guibg':'NONE', 'cterm':'NONE'}],
                \ ['green'  , {'ctermfg' :'2' , 'ctermbg':'NONE', 'guifg':'NONE', 'guibg':'NONE', 'cterm':'NONE'}],
                \ ['lime'   , {'ctermfg' :'10', 'ctermbg':'NONE', 'guifg':'NONE', 'guibg':'NONE', 'cterm':'NONE'}],
                \ ['olive'  , {'ctermfg' :'3' , 'ctermbg':'NONE', 'guifg':'NONE', 'guibg':'NONE', 'cterm':'NONE'}],
                \ ['yellow' , {'ctermfg' :'11', 'ctermbg':'NONE', 'guifg':'NONE', 'guibg':'NONE', 'cterm':'NONE'}],
                \ ['navy'   , {'ctermfg' :'4' , 'ctermbg':'NONE', 'guifg':'NONE', 'guibg':'NONE', 'cterm':'NONE'}],
                \ ['blue'   , {'ctermfg' :'12', 'ctermbg':'NONE', 'guifg':'NONE', 'guibg':'NONE', 'cterm':'NONE'}],
                \ ['teal'   , {'ctermfg' :'6' , 'ctermbg':'NONE', 'guifg':'NONE', 'guibg':'NONE', 'cterm':'NONE'}],
                \ ['aqua'   , {'ctermfg' :'14', 'ctermbg':'NONE', 'guifg':'NONE', 'guibg':'NONE', 'cterm':'NONE'}]]
    endif


    if !exists("b:syntaXAttrTags")
        let b:syntaXAttrTags=[["\\*","\\*", 'italic'], ["_","_", 'italic'], ["\\*\\*", "\\*\\*", 'bold'], 
                \ ["__","__", 'bold']]
    endif
    
    if !exists("b:syntaXstart")
        let b:syntaXstart="<span[^>]*style=\"\\([^;]*;\\)*color:@NAME@[^\"]*\"[^>]*>"
    endif 

    if !exists("b:syntaXend")
        let b:syntaXend="</span>"
    endif 

    if !exists("b:syntaXColorAttr")
        let b:syntaXColorAttr={'bold':';font-weight:bold', 'italic':';font-weight:italic', 
                \ 'strikethrough':';text-decoration:line-through', 
                \ 'underline':';text-decoration:underline'}
    endif
        
    if !exists("b:syntaXdelim")
        let b:syntaXdelim="+"
    endif
    "let g:syntaXstart="<span[^>]*style=\"[^;]*[;\\(color:@NAME@\\))]\+[^;]*;*[;(color:red)|(font\-weight:bold)]\+[^\"]\"[^>]*>"
    "let g:syntaXBoldstart="<span[^>]*style=\"\\([^;]*;\\)*color:@NAME@[^\"]*\"[^>]*>"

endfunction

function! <SID>syntaXAdd()
    echo "syntax#Add: todo"
endfunction

function! <SID>syntaXRemovehl()
    echo "syntax#Remove: todo"
endfunction

function! <SID>syntaXRemove()
    " construct regex list
    " find and highlight
    " ask remove
    " remove if yes
    echo "syntax#Remove: todo"
endfunction


