if exists("b:syntaX")
    finish
else
    let b:syntaX=1
endif

command! SyntaXEnable :call syntaX#Extend()
command! SyntaXList :call syntaXList#ListSettings()
command! SyntaXRemove :call syntaX#Remove()
